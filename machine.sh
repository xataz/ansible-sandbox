#!/bin/bash

script_name=$0

function f_help() {
    argv=$1

    case $argv in
        build)
            echo "Build image"
            echo "Arguments :"
            echo "    -o, --os : OS name"
            echo "    -v, --version : Version of OS"
            echo "Exemple :"
            echo "    Build all ubuntu : $script_name build -o ubuntu"
            echo "    Build debian buster : $script_name build -o debian -v 10"
            echo "    Build all Dockerfile : $script_name build"
        ;;
        create)
            echo "Create container"
            echo "Arguments :"
            echo "    -o, --os : OS name (default: debian)"
            echo "        special value: "
            echo "              - all : While on all image available"
            echo "              - random : choose randomly"
            echo "    -v, --version : Version of OS (default: latest)"
            echo "    -c, --count : Number of occurence (default: 1)"
            echo "    -p, --project : Project name (default: sandbox)"
            echo "Exemple :"
            echo "    Create 4 centos 8 : $script_name create -o centos -v 8 -c 4"
            echo "    Create 2 occurences of all image with project name ansible: $script_name build -o all -c 4 -p ansible"
            echo "    Create 10 randoms containers : $script_name build -o random -c 10"
            echo "    Create 2 randoms containers with latest tag : $script_name build -o random -v latest -c 2"
        ;;
        delete)
            echo "Delete project"
            echo "Arguments :"
            echo "    -p, --project : OS name (default: sandbox)"
            echo "Exemple :"
            echo "    delete ansible project : $script_name delete --project ansible"
        ;;
        ssh)
            echo "Connect to container"
            echo "Arguments :"
            echo "    -i, --id : id of container (default: 1)"
            echo "    -p, --project: Project name (default: sandbox)"
            echo "Exemple :"
            echo "    ssh on first container of ansible project: $script_name ssh -i 1"
        ;;
        list)
            echo "List container from project"
            echo "Arguments :"
            echo "    -p, --project : OS name (default: sandbox)"
            echo "Exemple :"
            echo "    list ansible project : $script_name list --project ansible"
        ;;
        gen)
            echo "Gen inventory for ansible from project"
            echo "Arguments :"
            echo "    -p, --project : OS name (default: sandbox)"
            echo "    -f, --format: Format of export (default: ini)"
            echo "Exemple :"
            echo "    Gen ansible inventory : $script_name gen --format yaml -p test"
        ;;
        genkey)
            echo "Gen ssh key"
            echo "Exemple :"
            echo "    Gen ansible inventory : $script_name genkey"
        ;;
        *)
        echo "build : build image"
        echo "create : run container(s)"
        echo "delete : delete project"
        echo "ssh : connect to container"
        echo "list : list container of project"
        echo "gen : gen ansible inventory"
        echo "genkey : gen ssh key"
        ;;

    esac
}


function f_build() {
    argc=$#
    argv=$@
    counter=0
    os="*"
    version="*"

    while [ $counter -lt $argc ]; do
        case $1 in
            -o|--os)
                shift
                os=${1}
                counter=$(($counter+1))
                shift
            ;;
            -v|--version)
                shift
                version=${1}
                counter=$(($counter+1))
                shift
            ;;
            -h|--help)
                f_help build
                exit 0
            ;;
            *)
                f_help build
                exit 1
            ;;
        esac
        counter=$(($counter+1))
    done
       
    for dockerfile in $(find ./${os} -type f -name "Dockerfile.${version}"); do
        image_name=$(basename $(dirname $dockerfile))
        image_version=$(basename $dockerfile | cut -d"." -f2-)
        image_folder=$(dirname $dockerfile)
        docker build -t ${image_name}-sandbox-init:${image_version} -f ${dockerfile} .
    done
}


f_create() {
    argc=$#
    argv=$@
    counter=0
    number=1
    project=sandbox

    while [ $counter -lt $argc ]; do
        case $1 in
            -o|--os)
                shift
                os=${1}
                counter=$(($counter+1))
                shift
            ;;
            -v|--version)
                shift
                version=${1}
                counter=$(($counter+1))
                shift
            ;;
            -n|--name)
                shift
                name=${1}
                counter=$(($counter+1))
                shift
            ;;
            -p|--project)
                shift
                project=${1}
                counter=$(($counter+1))
                shift
            ;;
            -c|--count)
                shift
                number=${1}
                counter=$(($counter+1))
                shift
            ;;
            -h|--help)
                f_help create
                exit 0
            ;;
            *)
                f_help create
                exit 1
            ;;
        esac
        counter=$(($counter+1))
    done
    if [ "${os}" == "" ]; then
        os=debian
    fi

    if docker ps --format '{{ .Names }}' | grep -E "^${project}" > /dev/null 2>&1; then
        echo "Project already exist"
        exit 1
    fi
    


    if [ "${os}" == "all" ]; then
        if [ "${version}" == "latest" ]; then
            counter=1
            counter2=1
            while [ $counter -le $number ]; do
                for image in $(docker images *-sandbox-init | grep -v "REPOSITORY" | awk '{print $1":"$2}' | grep latest); do
                    image_name_short=$(echo $image | cut -d- -f1)
                    docker run -dt --name ${project}-${image_name_short}-${counter2} --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro $image
                    counter2=$(($counter2+1))
                done
                counter=$(($counter+1))
            done
        else
            counter=1
            counter2=1
            while [ $counter -le $number ]; do
                for image in $(docker images *-sandbox-init | grep -v "REPOSITORY" | awk '{print $1":"$2}' | grep -v latest); do
                    image_name_short=$(echo $image | cut -d- -f1)
                    image_tag=$(echo $image | cut -d: -f2)
                    docker run -dt --name ${project}-${image_name_short}-${image_tag}-${counter2} --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro $image
                    counter2=$(($counter2+1))
                done
                counter=$(($counter+1))
            done
        fi
    elif [ "${os}" == "random" ]; then
        if [ "${version}" == "latest" ]; then
            counter=1
            for image in $(docker images *-sandbox-init | grep -v "REPOSITORY" | awk '{print $1":"$2}' | grep latest | shuf -n $number -r); do
                image_name_short=$(echo $image | cut -d- -f1)
                image_tag=$(echo $image | cut -d: -f2)
                docker run -dt --name ${project}-${image_name_short}-${image_tag}-${counter} --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro $image
                counter=$(($counter+1))
            done
        else
            counter=1
            for image in $(docker images *-sandbox-init | grep -v "REPOSITORY" | awk '{print $1":"$2}' | grep -v latest | shuf -n $number -r); do
                image_name_short=$(echo $image | cut -d- -f1)
                image_tag=$(echo $image | cut -d: -f2)
                docker run -dt --name "${project}-${image_name_short}-${image_tag}-${counter}" --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro $image
                counter=$(($counter+1))
            done
        fi 
    else
        counter=1
        if [ "${version}" == "" ]; then
            version=latest
        fi
        while [ $counter -le $number ]; do
                docker run -dt --name ${project}-${os}-${version}-${counter} --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro $os-sandbox-init:$version
                counter=$(($counter+1))
        done 

    fi

    echo "All machines are generated : "
    for container in $(docker ps --format '{{ .Names }}' | grep -E "^$project" | tac); do
        
        echo "      $container ==> $(docker inspect $container --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}')"
    done
}

f_delete() {                                                                                                                                                                   
    argv=$@
    argc=$#
    project=sandbox                                                                                                                                                                  

    if [ $argc -ne 0 ]; then                                                                                                                                                             
        case $1 in                                                                                                                                                                 
            -h|--help)                                                                                                                                                                    
                f_help delete                                                                                                                                                         
                exit 0                                                                                                                                                             
            ;;      
            -p|--project) 
                shift                                                                                                                                                                    
                project=$1                                                                                                                                                         
            ;;                                                                                                                                                                
            *)                                                                                                                                                                    
                f_help delete                                                                                                                                                         
                exit 1                                                                                                                                                             
            ;;                                                                                                                                                                     
                                                                                                                                                                    
        esac
    fi
                                                                                                                                  
    for container in $(docker ps --format '{{ .Names }}' | grep -E "^$project"); do
        docker container rm -f ${container}
    done                                                                                                    
}                                                                                                                                                                              
                                                                                                                                                                                
 f_list() {                                                                                                                                                                       
    argv=$@                                                                                                                                                                    
    argc=$#
    project=sandbox

    if [ $argc -ne 0 ]; then
        case $1 in                                                                                                                                                                 
            -p|--project)   
                shift                                                                                                                                                                  
                project=$1                                                                                                                                                         
            ;;      
            -h|--help)                                                                                                                                                                    
                f_help delete                                                                                                                                                            
                exit 0                                                                                                                                                             
            ;;                                                                                                                                                                 
            *)                                                                                                                                                                    
                f_help delete                                                                                                                                                         
                exit 1                                                                                                                                                             
            ;;                                                                                                                                                                     
                                                                                                                                                                            
        esac    
    fi                                                                                                                                                                   
                                                                                                                                                                                
    for container in $(docker ps --format '{{ .Names }}' | grep -E "^$project" | tac); do                                                                                               
        echo "$container ==> $(docker inspect $container --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}')"                                                                                                                                          
    done                                                                                                                                                                       
 }  

 f_gen() {
    argv=$@                                                                                                                                                                    
    argc=$#
    format=ini
    counter=0
    project=sandbox

    while [ $counter -lt $argc ]; do            
        case $1 in                                                                                                                                                                                                                                                                                                                             
            -p|--project) 
                shift                                                                                                                                                                    
                project=$1   
                counter=$(($counter+1))
                shift                                                                                                                                                      
            ;;         
            -h|--help)                                                                                                                                                                    
                f_help delete
                counter=$(($counter+1))                                                                                                                                                            
                exit 0                                                                                                                                                             
            ;;           
            -f|--format)
                shift
                format=$1
                counter=$(($counter+1))
                shift
            ;;                                                                                                                                                      
            *)                                                                                                                                                                    
                f_help delete                                                                                                                                                         
                exit 1                                                                                                                                                             
            ;;                                                                                                                                     
        esac
        counter=$(($counter+1))
    done                                                                                                                                                                      

    if [ "$format" == "ini" ]; then
        echo "[$project]" > ${project}_inventory                                                                                                                                                                            
        for container in $(docker ps --format '{{ .Names }}' | grep -E "^$project" | tac); do                                                                                               
            echo "$container ansible_host=$(docker inspect $container --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}')" >> ./${project}_inventory                                           
        done   
    elif [ "$format" == "yaml" ] || [ "$format" == "yml" ]; then
        echo "all:" > ${project}_inventory.yml
        echo "  hosts:" >> ${project}_inventory.yml
        echo "    children:" >> ${project}_inventory.yml
        echo "      ${project}:" >> ${project}_inventory.yml
        for container in $(docker ps --format '{{ .Names }}' | grep -E "^$project" | tac); do                                                                                               
            echo "        ${container}: " >> ./${project}_inventory.yml
            echo "          ansible_host: $(docker inspect $container --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}')" >> ./${project}_inventory.yml 
        done 
    fi
}

f_ssh() {
    argc=$#
    argv=$@
    counter=0
    project="sandbox"
    id=1

    while [ $counter -lt $argc ]; do
        case $1 in
            -p|--project)
                shift
                project=${1}
                counter=$(($counter+1))
                shift
            ;;
            -i|--id)
                shift
                id=${1}
                counter=$(($counter+1))
                shift
            ;;
            -h|--help)
                f_help build
                exit 0
            ;;
            *)
                f_help build
                exit 1
            ;;
        esac
        counter=$(($counter+1))
    done
    container=$(docker ps --format '{{ .Names }}' | grep -E "^${project}-.*-${id}$")
    if [ "$container" == "" ]; then
        f_help build
        exit 1
    else
        ip=$(docker inspect $container --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}')
        ssh -o "StrictHostKeyChecking=no" ansible@$ip
    fi
}

f_genkey() {
    ssh-keygen -t rsa -f ~/.ssh/id_ansible -N ""
    cp ~/.ssh/id_ansible.pub .
}

if [ ! -e ./id_ansible.pub ]; then
    echo "Please generate ssh key"
    echo "$! genkey"
fi


case $1 in
    build)
        shift
        f_build $@
    ;;
    create)
        shift
        f_create $@
    ;;
    ssh)
        shift
        f_ssh $@
    ;;
    delete)
        shift
        f_delete $@
    ;;
    ls|list)
        shift
        f_list $@
    ;;
    gen)
        shift
        f_gen $@
    ;;
    check)
        shift
        f_check
    ;;
    genkey)
        shift
        f_genkey
    ;;
    help)
        shift
        f_help $@
    ;;
    *)
        shift
        f_help
    ;;
esac
