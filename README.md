# Build image
```
Arguments :
    -o, --os : OS name
    -v, --version : Version of OS
Exemple :
    Build all ubuntu : ./machine.sh build -o ubuntu
    Build debian buster : ./machine.sh build -o debian -v 10
    Build all Dockerfile : ./machine.sh build
```

# Create container
```
Arguments :
    -o, --os : OS name (default: debian)
        special value: 
              - all : While on all image available
              - random : choose randomly
    -v, --version : Version of OS (default: latest)
    -c, --count : Number of occurence (default: 1)
    -p, --project : Project name (default: sandbox)
Exemple :
    Create 4 centos 8 : ./machine.sh create -o centos -v 8 -c 4
    Create 2 occurences of all image with project name ansible: ./machine.sh build -o all -c 4 -p ansible
    Create 10 randoms containers : ./machine.sh build -o random -c 10
    Create 2 randoms containers with latest tag : ./machine.sh build -o random -v latest -c 2
```

# List container from project
```
Arguments :
    -p, --project : OS name (default: sandbox)
Exemple :
    list ansible project : ./machine.sh list --project ansible
```

# Connect to container
```
Arguments :
    -i, --id : id of container (default: 1)
    -p, --project: Project name (default: sandbox)
Exemple :
    ssh on first container of ansible project: ./machine.sh ssh -i 1
```

# Delete project
```
Arguments :
    -p, --project : OS name (default: sandbox)
Exemple :
    delete ansible project : ./machine.sh delete --project ansible
```

# Gen inventory for ansible from project
```
Arguments :
    -p, --project : OS name (default: sandbox)
    -f, --format: Format of export (default: ini)
Exemple :
    Gen ansible inventory : ./machine.sh gen --format yaml -p test
```

# Gen ssh key
```
Exemple :
    Gen ansible inventory : ./machine.sh genkey
```